package top.bujiaban.stream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StreamDemoApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(StreamDemoApplication.class);
        application.run(args);
    }
}