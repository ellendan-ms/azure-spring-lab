package top.bujiaban.stream.service;

import com.azure.spring.messaging.checkpoint.Checkpointer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import java.util.function.Consumer;

import static com.azure.spring.messaging.AzureHeaders.CHECKPOINTER;

@Slf4j
@Component
public class ServiceBusConsumer {
    @Bean
    public Consumer<Message<String>> consume() {
        return message -> {
            Checkpointer checkpointer = (Checkpointer) message.getHeaders().get(CHECKPOINTER);
            log.info("New message received: '{}'", message.getPayload());
            checkpointer.success()
                    .doOnSuccess(s -> log.info("Message '{}' successfully checkpointed", message.getPayload()))
                    .doOnError(e -> log.error("Error found", e))
                    .block();
        };
    }
}
