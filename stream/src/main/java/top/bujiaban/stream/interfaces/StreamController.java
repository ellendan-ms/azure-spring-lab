package top.bujiaban.stream.interfaces;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Sinks;
import top.bujiaban.stream.interfaces.request.PersonDTO;
import top.bujiaban.stream.service.ServiceBusProducerConfiguration;

@Slf4j
@Data
@RestController
public class StreamController {
    private final Sinks.Many<Message<String>> many;
    @PostMapping(value = "/stream")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public ResponseEntity<String> send(@RequestBody PersonDTO request) {
        many.emitNext(MessageBuilder.withPayload(request.toString()).build(),
                Sinks.EmitFailureHandler.FAIL_FAST);
        return ResponseEntity.ok("Sent!");
    }
}
