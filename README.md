### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

### Development
Command:
```
./mvnw -U clean spring-boot:run
```

### Appidx
[Spring Cloud Azure](https://learn.microsoft.com/zh-cn/azure/developer/java/spring-framework/spring-cloud-azure?tabs=maven)
[azure-spring-boot-samples](https://github.com/Azure-Samples/azure-spring-boot-samples/tree/azure-spring-cloud_2.14.0/servicebus/azure-spring-cloud-stream-binder-servicebus-topic/servicebus-topic-binder)